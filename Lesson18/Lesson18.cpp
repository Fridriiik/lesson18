#include <iostream>
#include <ctime>

template <typename StackType>
class Stack{
public:
	//removing an item from the stack and return
	//LastElement - pointer to return value
	void Pop(StackType* LastElement) {
		if (StackLength <= 0) {
			std::cout << "There are no items in the stack" << '\n';

			//���� ���������� ��� ������, ������ �������������� ������ � ����������� ��� ����� ������. 
			//������� ���� �� �������� ������ _crtisvalidheappointer(block) ����� ����������� ����� �������� ����� ������� ��������. 
			StackData = new StackType[StackLength];


			return;
		}
		
		*LastElement = StackData[StackLength - 1];

		StackLength--; 

		tmp = (StackType*)realloc(StackData, StackLength * sizeof(StackType));
		if (tmp != NULL) {
			StackData = tmp;
		}
	}

	//just remove the element
	void Pop() {
		if (StackLength <= 0) {
			std::cout << "There are no items in the stack" << '\n';

			//���� ���������� ��� ������, ������ �������������� ������ � ����������� ��� ����� ������. 
			//������� ���� �� �������� ������ _crtisvalidheappointer(block) ����� ����������� ����� �������� ����� ������� ��������. 
			StackData = new StackType[StackLength];
			return;
		}

		StackLength--;


		
		tmp = (StackType*)realloc(StackData, StackLength * sizeof(StackType));

		if (tmp != NULL) {
			StackData = tmp;
		}
	}

	//Adding an item to the stack
	//NewElement - the value to add to the stack
	void Push(StackType NewElement) {
		StackLength++;

		tmp = (StackType*)realloc(StackData, StackLength * sizeof(StackType));

		if (tmp != NULL) {
			StackData = tmp;
		}

		StackData[StackLength - 1] = NewElement;
	
	}

	//Read the last item on the stack
	StackType Peek() {
		if (StackLength <= 0) {
			std::cout << "There are no items in the stack" << '\n';
			return NULL;
		}

		return StackData[StackLength - 1];
	}

	StackType Length() {
		//����� ���������� ���������� StackLength, �� � ����� ������ �������� ����������� ������� � ������. 
		//������������������ �����������, ����� �� �������� � ���������� �������������, �� ��� �� �����
		if (StackLength > 0) {
			return _msize(StackData) / sizeof(StackType);
		}
		else {
			return 0; 
		}
	}

	//destructor
	~Stack() {
		if (StackData !=  NULL) {
			delete[] StackData;
		}
	}

private:
	int StackLength = 0; 
	StackType* StackData = new StackType[StackLength];
	StackType* tmp = NULL;
};

int main()
{
	srand(static_cast<int>(time(0)));

	Stack <double> MyStack;

	double element = 0;

	for (int i = 0; i < 15; i++) {
		MyStack.Push(static_cast<double>(rand()) / 7);
		std::cout << MyStack.Peek() << "\n";
	}

	std::cout << "-----------" << "\n";
	MyStack.Pop(&element);

	std::cout << element << "\n";
	std::cout << MyStack.Peek() << "\n";

	for (int i = 14; i > 5; i--) {
		MyStack.Pop();
	}

	std::cout << MyStack.Peek() << "\n";

	std::cout << "Lenght - " << MyStack.Length() << "\n";

	for (int i = 20; i > 0; i--) {
		MyStack.Pop();
	}

	std::cout << "Lenght - " << MyStack.Length() << "\n";
	std::cout << "-----------" << "\n";

	for (int i = 0; i < 15; i++) {
		MyStack.Push(static_cast<double>(rand()) / 7);
		std::cout << MyStack.Peek() << "\n";
	}

	std::cout << MyStack.Peek() << "\n";
	std::cout << "Lenght - " << MyStack.Length() << "\n";
	
}